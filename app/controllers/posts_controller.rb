class PostsController < ApplicationController
  def index
    current_user_id = current_user[:id]
    @posts = Post.all
    friend_query = Friendship.where("friendships.friend_a = #{current_user_id} OR friendships.friend_b = #{current_user_id}")
    friend_id = friend_query.map do |value|
      value[:friend_a].to_s == current_user_id.to_s ? value[:friend_b] : value[:friend_a]
    end
    @users = User.where.not(id: [friend_id])
    @friends = User.where(id: friend_id )
  end

  def create
    puts params
    post = Post.new
    post.contents = params[:contents]
    post.user_id  = current_user[:id] 
    post.name     = current_user[:name]
    post.save
    @posts = Post.all
    @users = User.all
    @friends = []    
    render :index
  end

  def delete
    Post.find_by(id: params[:id]).destroy
    @posts = Post.all
    @users = User.all  
    @friends = []  
    render :index
  end

  def update
    post = Post.find_by(id: params[:id])
    post.contents = params[:contents]
    post.save
    @posts = Post.all
    @users = User.all    
    @friends = []
    render :index
  end

  def add_friend
    friend = Friendship.new
    friend.friend_a = params[:friend_a]
    friend.friend_b = params[:friend_b]
    friend.save
    @posts = Post.all
    @users = User.all    
    redirect_to('/')
  end

  def unfriend
    id = params[:id]
    current_user_id = current_user[:id] 
    find_id = Friendship.where(friend_a: id, friend_b: current_user_id).or(Friendship.where(friend_a: current_user_id, friend_b: id))
    Friendship.find_by(id: find_id).destroy
    @posts = Post.all
    @users = User.all    
    redirect_to('/')
  end
end

# export GEM_HOME=~/.gem
# export GEM_PATH=~/.gem
# https://tandahq.github.io/manila-rails-bootcamp-2018/