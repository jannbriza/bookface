class CreateFriendships < ActiveRecord::Migration[5.2]
  def change
    create_table :friendships do |t|
      t.string :friend_a
      t.string :friend_b
    end
  end
end
